# Event Communication

How do you communication changes across an application. How do you communicate state over time? RxJS, please.

Observables are perfectly suited to this.  We already know how to consume data over time, now we need to know how to control the initial output.

RxJS `Subject` allows us to both consume information via an observable (it has an observable in it) and also **broadcast** or pass data along the observable stream, via a subject. We can use this as an event bus to communicate state across the application.

Example:

```ts
import { Injectable } from '@angular/core';
import { Subject }    from 'rxjs/subject';

@Injectable()
export class NotificationsService {
  private subject = new Subject(); // can broadcast and consume
  notifications$ = this.subject.asObservable(); // create observable stream

  /**
   * When called with a notification, we turn around and call the Observable
   * .next() method.
  */
  emit(notification) {
    this.subject.next(notification);
  }
}
```

That's the whole event bus!

To use it, we first inject the `NotificationsService` into the main `app.component.ts`:

```ts
export class AppComponent implements OnInit {

  constructor(private snackbar: MdSnackBar,
              private ns: NotificationsService) {}

  ngOnInit() {
    this.ns.notifications$
      .subscribe(notification => this.showNotification(notification)):
  }

  showNotification(notification) {
    this.snackbar.open(notification, 'OK', {
      duration: 3000
    });
  }

}
```

And then emit events within some other component. The author uses it in a hypothetical newsletter component:

```ts
export class AppNewsletter implements OnInit {

  // inject dependencies
  constructor(private fb: FormBuilder,
              private ns: NotificationsService) {}

  // initialize form
  ngOnInit() {
    this.subscriber = this.fb.group({
      name  : ['', Validators.required],
      email : ['', Validators.required]
    })
  }

  // class method that just emits NotificationsService events
  subscribe({ value, valid }: { value: Subscriber, valid: boolean }) {
    // if valid ...
    this.ns.emit(`${value.name} just subscribed!`);
  }

}
```