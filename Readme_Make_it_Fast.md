# Make it Fast

In author's experience, more often than not, if you have a performance problem you also have a UX problem. The performance problem is a trailing indicator that your experience is not optimal.

If you're running into a performance problem, first stop and look at the user experience. Is there a way to both improve performance and improve the user experience?

### Change Detection

Angular 2+ re-did change detection vs Angular 1. They also give us control over when and where change detection happens.

Brian Ford, former Angular team member, wrote Zone.JS. Seems like virtual DOM. Monkey patching entire DOM structure in application. Analyze app, creates optimized class with current and previous state. If the two are ever different, it knows it needs to do something about it. This change to change detection translated to a 3-10x performance increase.

Faster than that is if you simply didn't have to do change detection at all.

Change Detection Strategy

* We can control how an Angular node will respond when it detects changes within the application.
* By default, Angular will always detect changes and respond on all nodes.
* We can set the change detection strategy to `onPush` meaning that Angular will only check for changes once and then it's done. This is essentially turning off change detection for a single component branch (a node and its children).
* Turning off change detection for a component branch in our application has serious performance implications.

Observables are totally capable of handling their own change detection. That's what `.next()` is for. So, when using observables, we do not need Angular's own change detection - it's already in the context of the observable.

So in a component:

```ts
@Component({
  selector        : 'app-widgets',
  templateUrl     : './widgets.component.html',
  styleUrls       : ['./widgets.component.css'],
  changeDetection : ChangeDetectionStrategy.onPush
})
export class WidgetsComponent implements OnInit() { }
```

Caveat: if you have an observable stream, and you set it to something, this will not update the template. Why not? Because we turned off change detection. So if we need to update a template, we call `detectChanges()` within the class:

```ts
export class WidgetsComponent implements OnInit() {
  
  constructor(private cd: ChangeDetectorRef) {}

  ngOnIniti() {
    Observable.fromEvent(document, 'click')
      .map(event => 100)
      .startWith(5)
      .subscribe(coolness => {
        this.coolness = coolness;
        this.cd.detectChanges();  // <-- see here!
      });
  }
}
```

Allowing observables to handle eventing is a performance win. And *we* control change detection.

From within a template, if you're binding heavily to a bunch of internal logic, or doing a lot of data manipulation outside of the observable stream, you're not going to want to turn off Angular's change detection. But for the most part, all the data coming into a component is going to be handled by an observable stream. In which case, it's easy to turn off Angular change detection and use `detectChanges()`. This is the author's choice, for performance reasons.