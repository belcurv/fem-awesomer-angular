# Lazy Loaded Modules

To help with creating better performance in Angular applications, only load a module with its assets and resources when it's actually needed.

Say a user spends 80% of their time on a single feature of your application, it may make sense to make the other app features lazy. The user never has to incur the overhead of loading those thing, making their UX that much faster.

This is surprisingly easy to do in Angular.

### Step 1: wrap a componenent in a module

Wrap the component you want to lazy load within its own module, complete with routing. You set up a route just like you would in a normal module.

Child Module:

```ts
const routes: Routes = [
  { path: '', component: ProfileComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UserRoutingModule { }
```

Then, in our main parent app module, instead of saying "go to this component", we say `loadChildren`:

```ts
const routes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'items', component: ItemsComponent },
  { path: 'widgets', component: WidgetsComponent },
  { path: 'profile', loadChildren: './user/user.module#UserModule' }, // <--
  { path: '**', redirectTo: '', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  providers: []
})
export class AppRoutingModule { }
```

### How do?

CLI

```
ng g m refreshments --routing 
```