# Template Driven Forms

**Templates**

Data goes into events via property binding, data comes out of templates via event binding.

How do components and directives differ: components have templates; directives modify an existing element (`*ngFor`, `*ngIf`, etc).

How is a service like a component: they're both ES6 classes, with metadata, constructors, properties and methods.

How do they differ? Services are literally just a class and most often will only use an `@Injectable` decorator. Services just do straight business-logic-y things. They have no template. They also have no lifecycle hooks.

### Template Driven Forms

Goal is to take a widget from a list and push the data into a form.

Step 1: getting `FormsModule` into our app. This is very easy - the CLI does this for us.
Step 2: Form Controls - understadning how Angular is controlling the form under the hood.
Step 3: Ways to validate or style forms based on their validation state (micro interactions).

To use forms in an Angular application, we simply need to add the `FormsModule` to `ngModule` imports section. CLI does this for us. But if we were doing this by hand:

```ts
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
```

**Must** install the npm dependency before we can use forms. Apparently, all of Angular comes in pieces.

```
"@angular/animations": "^4.4.1",
"@angular/cdk": "^2.0.0-beta.10",
"@angular/common": "^4.3.6",
"@angular/compiler": "^4.3.6",
"@angular/compiler-cli": "^4.3.6",
"@angular/core": "^4.3.6",
"@angular/forms": "^4.3.6",
"@angular/http": "^4.3.6",
"@angular/material": "^2.0.0-beta.10",
"@angular/platform-browser": "^4.3.6",
"@angular/platform-browser-dynamic": "^4.3.6",
"@angular/router": "^4.3.6",
```

Angular team has broken up the functionality of the core into smaller pieces that we can use as we see fit. For example, in a kiostk app, where we're simply displaying data, there's no reason to import forms.

### ngModel

* Enables 2-way data binding within a form.
* Creates a `FormControl` instance from a domain model and binds it to a form element.
* We can create a local varaible to reference the `ngModel` instance of the element.

Need to track 2 things with every form: the **state** of the form (is it valid? Not valid? How is it invalid?), and the **data** of the form.

Under the hood, `ngModel` creates a form control. It takes the domain model, whatever that is, and binds it to a form element.

Remember, `#someLocalVar` -- the `#` creates a local variable within the template.

```html
<input [(ngModel)]="selectedItem.name"
  name="name" #nameRef="ngModel"
  placeholder="Enger a name"
  type: text">
```

Another example. We create local variable `#nameInput` and use to to output two `formControl` properties: `value` and `valid`. Is the form valid and what data makes it so?

```html
<md-card-content>
  <md-input-container class="full-width">
    <input
      #nameInput="ngModel" required
      mdInput placeholder="Name"
      [(ngModel)]="selectedItem.name"
      type="text" name="name">
  </md-input-container>
  <hr>
  {{ nameInput.value }}
  <hr>
  {{ nameInput.valid }}
</md-card-content>
```

### Form Controls

* `ngControl` binds a DOM element to a `FormControl`
* `FormControl` is responsible for tracking value and validation status of a single form element
* You can group `FormControl`s together with `FormGroup`. More often than not, you have multiple inputs in your form. Group them together.
* `ngForm` binds an HTML form to a top-level `FormGroup`
* We can create a local variable to reference `ngForm` instance of a form
* `ngModelGroup` creates and binds a `FormGroup` instance to a DOM element.

Hierarchical: `formControl` at the bottom. You have `formGroup`s on top of that. You can also have `formArray` - an array of `formControl`s. And then you have a top-level form group.

`ngModel` is for a single control. `ngForm`, the form group - a group of form controls - is tracking the value and validation state of the entire form. Every `<form>` has a top-level form group that's tracking the state of the entire form.

```html
<form novalidate #formRef="ngForm">
  <div>
    <label>Item Name</label>
    <input [(ngModel)]="selectedItem.name"
      name="name" required
      placeholder="Enter a name" type="text">
  </div>
  <div>
    <label>Item Description</label>
    <input [(ngModel)]="selectedItem.description"
      name="description"
      placeholder="Enter a description" type="text">
  </div>
</form>
```

Forms **need** `name` properties on whatever you're `[(ngModel)]`-ing. Angular uses the `name` internally.

We'll usually just keep track of `.value` & `.valid` states via the top-level form group.

Right now with template driven forms, as we update form controls / inputs, we're directly changing the model. When we get to reactive forms, we won't do this - instead we'll have separation between the form and the model, only writing back to the model upon some predetermined event. Reactive forms are handy for data immutability.

Nesting form fields with `ngModelGroup`. With this, you can create a bit of hierarchy in the resulting object to maybe match the data structures in the back end.

```html
<form novalidate #formRef="ngForm">
  <fieldset ngModelGroup="user">
    <label>First Name</label>
    <input [(ngModel)]="user.firstName"
      name="firstName" required
      placeholder="Enter your first name" type="text">
    <label>Last Name</label>
    <input [(ngModel)]="user.lastName"
      name="lastName"
      placeholder="Enter your last name" type="text">
  </fieldset>
</form>

<!--
Results in:
{
  "user": {
    "firstName": "Test",
    "lastName": "Test"
  }
}
-->
```

### Validation Styles

Angular will automatically attach styles to a form element depending on its state:

* `ng-valid` - attached if element is in a valid state.
* `ng-invalid` - attached if the element is in an invalid state.
* `ng-pristine` - attached if it is in the same state we found it in
* `ng-untouched` - attached if we never touched it at all. So if a form element is empty, but a user hasn't interacted with it yet, we don't want to run validation.

```css
input.ng-invalid {
  border-bottom: 1px solid red;
}

input.ng-valid {
  border-bottom: 1px solid green;
}
```

Todd Motto has some really good articles on reactive forms and form validation.
