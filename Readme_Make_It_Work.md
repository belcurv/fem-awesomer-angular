# Make it Work

**Testing and Debugging**

>If you want to write good tests, write good code.

"It hasn't broken yet" is not a good reply to "how do you know it works?"

Author spends 20% writing code and 80% fixing it. Tests give us reasonable certainty that something works.

### Component Testing

Testing can be summarized with some basic patterns.

To some extent with unit testing (and a greater extend with end-to-end testing) you spend an awful amount of time just setting up the test environment. Once you can streamline some of these pain points, you can focus on the basic patterns and become effective in testing.

1. **Small methods are easier to test.** The smaller it is and the more precise it is in what it does, the easier it is to come up with assertions.

2. **Pure functions/methods are easier to test.** Functions that are free from side effects and exhibit referential transparency (same result returned from same parameters every single time) are easy to test - there's a clear "contract" of inputs and outputs.

3. **Don't use real services.** Instead, fake that shit. Use dependency injection and test doubles. Author often passes in an empty class - that's sometimes enough to satisfy the dependency. Or, if it makes a server call, mock that service out. You can alternately use stubs or spies - spy on the real service, asserting that it was indeed called with whatever correct parameters.

### Basic Angular Structure

A class wrapped in metadata. So, we can use two testing approaches.

1. We can use the Angular testing utilities and test our components in the context of an Angular application. This is most appropriate for component testing.

2. We can bypass the Angular wrapper and test the class itself in isolation. This is often the easiest method for testing services, pipes, etc. which are **just** classes.

### Testing Big Picture

1. Karma - test runner; ships with Angular CLI.

2. Jasmine - assertion library; ships with Angular CLI.

3. Testing Utilities - ships with Angular CLI.

4. Our Code.

### Testing with Karma

* Karma is the test runner that is used to execute Angular unit tests.
* You can manually instal and configure Karma.
* Karma is installed and configured by default when you create a project with the Angular CLI
* Karma is configured via the `karma.conf.js` file.
* Tests (specs) are identified with a `.spec.ts` naming convention.

### Debugging With Karma

* Use the developer console in the Karma browser window to debug your unit tests.
* If something is throwing an error, you will generally see it in the console.
* If you need to step through something, you can do so from a breakpoint in the developer tools.
* Logging to the console is also a handy tool for observing data and events.

### Basic Component Test

A basic component:

```ts
import { Component, OnInit } from '@angular/core';

@Component({
  selector    : 'app-gadget',
  templateUrl : './gadget.component.html',
  styleUrls   : ['./gadget.component.css']
})
export class GadgetComponent implements OnInit {
  title = 'Hello Gadget';

  constructor() { }

  ngOnInit() {
  }
}
```

A basic test:

```ts
import { async, ComponentFixture, DebugElement, TestBed } from '@angular/core/testing';

import { GadgetComponent } from './gadget.component';

describe('GadgetComponent', () => {
  let component : GadgetComponent;
  let fixture   : ComponentFixture<GadgetComponent>;
  let de: DebugElement;

  beforeEach(async(() => {
    TestBed
      .configureTestingModule({
        declarations: [ GadgetComponent ]
      })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GadgetComponent);
    component = fixture.componentInstance;
    de = fixture.debugElement;
    fixture.detectChanges();
  });

})
```

When writing a component test suite:

1. Configure the module - we have to create a module for the component to run in, so it can be tested. This is called the `TestBed`. This allows us to configure a testing module.

    `TestBed`

    * The most important piece of the Angular testing utilities.
    * Creates an Angular testing module which is an `@NgModule` class.
    * You can configure the module by calling `TestBed.configureTestingModule`.
    * Configure the testing module in the `BeforeEach` so that it gets reset before each spec.

2. Create a Fixture - context for the component to live in.

    `let fixture : ComponentFixture<GadgetComponent>;` says, "declare a ComponentFixture that wraps a GadgetComponent"

    `TestBed.createComponent(GadgetComponent)`

    * Creates an instance of the component under test.
    * Returns a component test fixture.
    * Calling `createComponent` closes the `TestBed` from further configuration.

3. Get Component Instance - 

    `ComponentFixture`

    * Handle to the test environment surrounding the component.
    * Provides access to the component itself via `fixture.componentInstance`.
    * Provides access to the `DebugElement` which is a handle to the component's DOM element.
    * `DebugElement.query` allows us to query the DOM of the element.
    * `By.css` allows us to construct our query using CSS selectors.

    From here, we can access properties and methods on the component. And that means we can write some assertions on them:

    ```ts
    it('should have a title', () => {
      expect(component.title).toBe('Hello Gadget');
    });
    ```

    Using `debugElement` you can do things like:

    ```ts
    it('should display a title', () => {
      const h1 = de.query(By.css('h1'));
      expect(h1.nativeElement.innerText).toBe('Hello Gadget');
    });
    ```

### Change Detection

Angular does not automatically handle change detection out of the box. This allows you to make an assertion, then make something change, and then test it. You can insert yourself in between change detection.

* We tell Angular to perform change detection by calling `ComponentFixture.detectChanges`.
* `TestBed.createComponent` does not automatically trigger a change detection.
* This is intentional as it gives us greater control over how we inspect our components pre-binding and post-binding.

```ts
it('should update the title', () => {
  component.title = 'Hello Developer!';
  fixture.detectChanges();

  const h1 = de.query(By.css('h1'));
  expect(h1.nativeElement.innerText).toBe('Hello Developer!');
});
```

### Component Testing Demo

Given a "FriesComponent" component:

```ts
import { Component, OnInit } from '@angular/core';

@Component({
  selector  : 'app-fries',
  template  : `<h1>{{ title }}</h1>`,
  styleUrls : ['./fries.component.css']
})
export class FriesComponent implements OnInit {
  title = 'Crack Fries are a thing!';

  constructor() { }

  ngOnInit() {
  }
}
```

A corresponding component test:

```ts
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { DebugElement } from '@angular/core';
import { By } from '@angular/platform-browser';

import { FriesComponent } from './gadget.component';

describe('FriesComponent', () => {
  let component : FriesComponent;
  let fixture   : ComponentFixture<FriesComponent>;
  let de        : DebugElement;

  beforeEach(async(() => {
    TestBed
      .configureTestingModule({
        declarations: [ FriesComponent ]
      })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture   = TestBed.createComponent(FriesComponent);
    component = fixture.componentInstance;
    de        = fixture.debugElement;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should have a title', () => {
    expect(component.title).toBe('Crack Fries are a thing!');
  });

  it('should display a title', () => {
    const h1 = de.query(By.css('h1'));
    expect(h1.nativeElement.innerText).toBe('Crack Fries are a thing!');
  });

  it('should update a title', () => {
    component.title = 'WITH CHILI!';
    fixture.detectChanges();
    const h1 = de.query(By.css('h1'));
    expect(h1.nativeElement.innerText).toBe('WITH CHILI!');
  });

})
```
