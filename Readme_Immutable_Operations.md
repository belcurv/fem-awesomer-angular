# Immutable Operations

When changing state, we give back a brand new object consisting of those changes. Why?

1. You don't affect other parts of the system.

2. You can revert back to previous states.

**Operators**

* `Object.freeze` - Makes an object effectively immutable. Prevents new properties from being added to an object; prevents existing properties from being removed; prevents existing properties (enumerability, configurability, writability) from being changed.
* Immutable Add
* Immutable Update
* Immutable Delete

Tools: `.concat()`, .`map()`, `.filter()`, `Object.assign()` - use these to move from mutable to immutable.

### Examples from _mutable_ to _immutable_

1. Pushing a value onto an existing state array is mutable:

```js
case CREATE_WIDGET:
  state.push(action.payload);
  return state;
```

To make it immutable we can used array spread or the `.concat()` method (mergest two or more arrays, returning a new array):

```js
case CREATE_WIDGET:
  return [...state, action.payload];

case CREATE_WIDGET:
  return state.concat(action.payload);
```

2. Updating an item in a collection:

Mutable:

```js
case UPDATE_WIDGET:
  state.forEach((widget, index) => {
    if (widget[comparator] === action.payload[comparator]) {
      state.splice(index, 1, action.payload);
    }
  });
  return state;
```

Immutable - use `.map()` and `Object.assign()`. Both return a new version of the thing:

```js
case UPDATE_WIDGET:
  return state.map((widget) => {
    return widget[comparator] === action.payload[comparator]
      ? Object.assign({}, action.payload) :
      : widget;
  });
```

3. Delete:

Mutable:

```js
case DELETE_WIDGET:
  state.forEach((widget, index) => {
    if (widget[comparator] === action.payload[comparator]) {
      state.splice(index, 1);
    }
  });
  return state;
```

Immutable - use `.filter()`:

```js
case DELETE_WIDGET:
  return state.filter(widget => {
    return widget[comparator] !== action.payload[comparator];
  });
```

