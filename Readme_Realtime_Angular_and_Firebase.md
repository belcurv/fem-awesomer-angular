# Make it Reatime: Angular and Firebase

It's surprisingly simple.

Marcus talk: "Go beast mode with realtime reactive interfaces in Angular 2 & Firebase", Angular London conference:

https://www.youtube.com/watch?v=5CTL7aqSvJU

### The Realtime Observable Stream

When we were catching events from the user (initial output), that's an example of a stream local to the application.

What happens when you put your "initial output" in a database that multiple people are connected to? It becomes real-time.

So, start with a realtime database. Firebase is one such database.

First things first, create a Firebase account. Supposedly they have a very generous free tier. Then create a project, which will generate a bunch of identifying keys/data that we need to hook up within Angular.

Add the generated details to: `environment.ts`

```ts
export const environment = {
  production: false,
  firebase: {
    apiKey            : '098nas0d98as0d98bas0d98basd',
    authDomain        : 'awesome-app.firebaseapp.com',
    databaseURL       : 'https://awesome-app.firebaseio.com',
    productId         : 'awesome-app',
    storageBucket     : '',
    messagingSenderId : '9876543210'
  }
};
```

Then we need to initialize some Firebase modules in our main app module:

```ts
imports: [
  AngularFireModule.initializeApp(environment.firebase),
  AngularFireDatabaseModule,
  BrowserAnimationsModule,
  BrowserModule,
  FormsModule,
  ReactiveFormsModule,
  HttpModule,
  AppRoutingModule,
  AppMaterialModule
],
```

`AngularFireModule.initializeApp(environment.firebase)` initializes Firebase module with our Firebase config from `environment.ts`.

### Consume the Real Time Stream

For example, in our notifications service:

```ts
// 1. define our notification stream
// This is just another service that maps our observable to an endpoint as
// an object.
notification$: FirebaseObjectObservable<Notification>;

constructor(db: AngularFireDatabase) {
  // 2. point the notification stream at our remote endpoint
  this.notification$ = db.object('/notification');
  // 3. subscribe to it.
  // Whenever some real-time things happens, kick it into the subject so 
  // we can multicast it to the rest of the application.
  this.notification$
    .skip(1) // The connection itself an event; we're not interested in that
    .subscribe(notification => this.subject.next(notification));
}
```

### Update the Real Time Stream

We use `.set()` at our observable stream:


```ts
notification$: FirebaseObjectObservable<Notification>;

constructor(db: AngularFireDatabase) {
  this.notification$ = db.object('/notification');
  this.notification$
    .skip(1) // The connection itself an event; we're not interested in that
    .subscribe(notification => this.subject.next(notification));
}

// update
emit(notification: Notification) {
  this.notification$.set(notification);
}
```
