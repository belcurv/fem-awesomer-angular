import { Injectable } from '@angular/core';

@Injectable()
export class CaffeineService {

  // property on our service
  sources = [
    'red bull',
    'monster',
    'coffee',
    'rockstar',
    '5 hour energy'
  ];

}
