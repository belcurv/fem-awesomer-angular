import { Component, OnInit } from '@angular/core';
import { WidgetsService, Widget } from '../shared';

@Component({
  selector    : 'app-widgets',
  templateUrl : './widgets.component.html',
  styleUrls   : ['./widgets.component.css']
})
export class WidgetsComponent implements OnInit {

  // interface! Typings!
  selectedWidget: Widget;
  widgets: Array<Widget>; // aka: widgets: Widget[];

  constructor(private widgetsService: WidgetsService) {}

  ngOnInit() {
    this.loadWidgets();
    this.reset();
  }

  loadWidgets() {
    this.widgetsService
      .all()
      .subscribe(widgets => this.widgets = widgets);
  }

  // Control how form is initialized.
  // Create an empty widget. Used every time the form loads.
  reset() {
    this.selectedWidget = { id: null, name: '', description: '' };
  }

  selectWidget(widget) {
    console.log('SELECTING WIDGET', widget)
    this.selectedWidget = widget;
  }

  saveWidget(widget) {
    if (!widget.id) {
      this.createWidget(widget);
    } else {
      this.updateWidget(widget);
    }
    this.reset();
  }

  createWidget(widget) {
    this.widgetsService
      .create(widget)
      .subscribe(() => {
        this.loadWidgets();
        this.reset();
      });
  }

  updateWidget(widget) {
    this.widgetsService
      .update(widget)
      .subscribe(() => {
        this.loadWidgets();
        this.reset();
      });
  }

  deleteWidget(widget) {
    this.widgetsService
      .delete(widget)
      .subscribe(() => {
        this.loadWidgets();
        this.reset();
      });
  }

  cancel(widget) {
    this.reset();
  }

}
