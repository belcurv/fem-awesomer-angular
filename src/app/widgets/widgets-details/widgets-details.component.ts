import { Component, Input, Output, EventEmitter } from '@angular/core';
import { Widget } from '../../shared';

@Component({
  selector    : 'app-widgets-details',
  templateUrl : './widgets-details.component.html',
  styleUrls   : ['./widgets-details.component.css']
})
export class WidgetsDetailsComponent {
  origWidget: Widget;
  widget: Widget;

  @Output() saved = new EventEmitter();
  @Output() cancelled = new EventEmitter();

  @Input() set selectedWidget(value: Widget) {
    if (value) { this.origWidget = value; }
    this.widget = Object.assign({}, value);
  }

}
