import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EnergydrinkComponent } from './energydrink.component';

describe('EnergydrinkComponent', () => {
  let component: EnergydrinkComponent;
  let fixture: ComponentFixture<EnergydrinkComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EnergydrinkComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EnergydrinkComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
