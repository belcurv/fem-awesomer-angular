import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EnergydrinkComponent } from './energydrink/energydrink.component';

const routes: Routes = [
  { path: '', component: EnergydrinkComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RefreshmentsRoutingModule { }
