import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RefreshmentsRoutingModule } from './refreshments-routing.module';
import { EnergydrinkComponent } from './energydrink/energydrink.component';

@NgModule({
  imports: [
    CommonModule,
    RefreshmentsRoutingModule
  ],
  declarations: [EnergydrinkComponent]
})
export class RefreshmentsModule { }
