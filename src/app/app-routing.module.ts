import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ItemsComponent } from './items/items.component';
import { HomeComponent } from './home/home.component';
import { WidgetsComponent } from './widgets/widgets.component';
import { ReviewComponent } from './review/review.component';
import { ItemComponent } from './items/item/item.component';

const routes: Routes = [
  { path: '', component: HomeComponent, data: { animation: 'home' }},
  { path: 'items', component: ItemsComponent, data: { animation: 'items' }},
  { path: 'item/:id', component: ItemComponent, data: { animation: 'item' }},
  { path: 'widgets', component: WidgetsComponent, data: { animation: 'widgets' }},
  { path: 'review', component: ReviewComponent, data: { animation: 'review' }},
  { path: 'refreshments', loadChildren: './refreshments/refreshments.module#RefreshmentsModule' },
  { path: '**', redirectTo: '', pathMatch: 'full'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  providers: []
})
export class AppRoutingModule {
}
