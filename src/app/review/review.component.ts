import { Component, OnInit } from '@angular/core';

@Component({
  selector    : 'app-review',
  templateUrl : './review.component.html',
  styleUrls   : ['./review.component.css']
})
export class ReviewComponent implements OnInit {

  name = 'YOLO';
  todos = [
    'learn angular',
    'master angular',
    'get job',
    'exel at job'
  ];

  constructor() {}

  ngOnInit() {}
}
