import { Component, OnInit } from '@angular/core';
import { Item, ItemsService } from '../../shared';
import { ActivatedRoute, ParamMap } from '@angular/router';

import 'rxjs/add/operator/switchMap';

@Component({
  selector    : 'app-item',
  templateUrl : './item.component.html',
  styleUrls   : ['./item.component.css']
})
export class ItemComponent implements OnInit {
  item: Item;

  constructor(
    private itemsService: ItemsService,
    private route: ActivatedRoute
  ) { }

  ngOnInit() {
    this.route.paramMap
      .switchMap((params: ParamMap) => this.itemsService.load(+params.get('id')))
      .subscribe(item => this.item = item);
  }

  saveItem(item) {
    // todo
  }

  cancel(item) {
    // todo
  }

}
