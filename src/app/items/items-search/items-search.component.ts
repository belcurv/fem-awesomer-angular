import { Component, EventEmitter, OnInit, Output, ViewChild } from '@angular/core';
import { ItemsService } from '../../shared';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/fromEvent';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/filter';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/operator/switchMap';

@Component({
  selector    : 'app-items-search',
  templateUrl : './items-search.component.html',
  styleUrls   : ['./items-search.component.css']
})
export class ItemsSearchComponent implements OnInit {

  /**
   * With @ViewChild we can capture a reference to some template element by
   * targeting a local variable defined in it - see: <input #itemsSearch ...
  */
  @ViewChild('itemsSearch') itemsSearch;
  @Output() results = new EventEmitter();

  constructor(private itemsService: ItemsService) { }

  ngOnInit() {

    /**
     * Capturing a DOM event and converting it into an observable.
     * We use the trailing $ sign by convention, to denote `search$` is an
     * observable.
     * You can't do this with simple binding.
    */
    const search$ = Observable
      .fromEvent(this.getNativeElement(this.itemsSearch), 'keyup')
      .debounceTime(200)
      .distinctUntilChanged()
      .map((event: any) => event.target.value)
      .map(query => query.toUpperCase())
      .switchMap(query => this.itemsService.search(query))
      .subscribe(items => this.results.emit(items));
  }

  getNativeElement(element) {
    return element.nativeElement;
  }

}
