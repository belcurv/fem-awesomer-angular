# Route Parameters

When an application has a collection of routes for specific containers or widgets, Lukas reviews how to go directly to a specific widget.

Quick Example:

```ts
const routes: Routes = [
  { path: '',          component: HomeComponent },
  { path: 'items',     component: ItemsComponent },
  { path: 'items/:id', component: ItemComponent },
  { path: 'widgets',   component: WidgetsComponent },
  { path: '**',        redirectTo: '', pathMatch: 'full },
];
```

Angular adopts the traditional `thing/:identifier` syntax, where the identifier is something dynamic.

Then, in the component class, on init we're actually watching the route `paramMap` observable:

```ts
export class ItemComponent implements OnInit {
  item: Item;

  constructor(
    private itemsService: ItemsService,
    private route: ActivatedRoute
  ) {}

  ngOnInit() {
    this.route.paramMap
      .switchMap((params: ParamMap) => this.itemsService.load(+params.get('id')))
      .subscribe(item => this.item = item);
  }
}
```

Angular wraps the URL route parameters in an observable. The route is reactive. In other words, when it changes, you can capture or subscribe to that observable stream.

So in the above, we subscribe to route changes, and using `switchMap` we map the route params stream to a brand new stream created by `itemsService`, which itself makes an XHR request and returns its own observable stream.

Look at how much is accomplished in 3 lines of code. State management, flow control and code volume - efficient.

Other than the `item` object, everything else exists in the stream.