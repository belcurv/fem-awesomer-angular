# Reactive Forms

HTML forms turn out to be some of the most verbose elements / markup you will ever code.

When you want your form to change dynamically based on selections or inputs within the form, doing this with template driven forms is very hard. You need a way to define / declare your form in your JS/TS and then from there you hook it into the template. Instead of letting the template drive the model, you let the model determine how the form is going to work.

In template-driven forms, everything is just sort of happening behind the scenes. You generally do not ineract with the `formGroup` or `formControl` directly. With reactive forms, you have complete access to them; they are pulled out and defined in your class.

Expanding on template-driven forms hierarchy

* `FormControl` - Defined in JS/TS. Tracks the value and validation status of an individual form control
* `FormGroup` - Defined in JS/TS. Tracks the value and validity state of a group of FormControl instances
* `formGroup` - In the template; directive binds an existing ForGroup to a DOM element
* `formControlName` - In the template; directive syncs a ForControl in an existing FormGroup to a form control element by name
* `formBuilder` - essentially syntactic sugar that shortens the `new FormGruop()`, `new FormControl()` and `new FromArray()` boilerplate that can build up in larger forms.

Demo

Have to import the reactive forms module in `NgModule` and declare it in the 'imports' section:

```ts
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

/* ... */

imports: [
  BrowserAnimationsModule,
  BrowserModule,
  FormsModule,
  ReactiveFormsModule,
```

`FormGroup` looks like this. Define a new FormGroup passing it an object that represents the form. Every field gets a property in the FormGroup:

```ts
form = new FormGroup({
  first : new FormControl('Nancy', Validators.minLength(2)),
  last  : new FormControl('Drew')
});
```

And bind to it in the template using the `[formGroup]` directive

```html
<form [formGroup]="form" (ngSubmit)="onSubmit()">
  <input formControlName="first" placeholder="First name" />
  <input formControlName="last" placeholder="Last name" />
  <button type="submit">Submit</button>
</form>
```

Then we have access to the form in a programmatic context. We can manipulate and interact with the form from a programmatic context.

```ts
export class SimpleFormGroup {

  form = new FormGroup({
    first : new FormControl('Nancy', Validators.minLength(2)),
    last  : new FormControl('Drew')
  });

  get first(): any {
    return this.form.get('first');
  }

  onSubmit(): void {
    console.log(this.form.value);  // { first: 'Nancy', last: 'Drew' }
  }

  setValue() {
    this.form.setValue({ first: 'Carson', last: 'Drew' });
  }

}
```

**Using FormBuilder**

To start, we define a FormGroup (`subscriber`) and inject FormBuilder:


```ts
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators} from '@angular/forms';

@Component({
  selector    : 'app-login',
  templateUrl : './login.component.html',
  styleUrls   : ['./login.component.css']
})
export class NewsLetterComponent implements OnInit {
  subscriber: FormGroup;

  constructor(private fb: FormBuilder) { }

  ngOnInit() {
    this.subscriber = this.fb.group({
      name  : ['', Validators.required],
      email : ['', Validators.required]
    });
  }
}
```

Then wire it up in the template. We bind to an Angular form group `[formGroup]="subscriber"`, and identify specific form controls (fields) using Angular's `formControlName` directives:

```html
<form novalidate [formGroup]="subscriber" (submit)="subscribe(subscriber)">
  
  <input type="text" placeholder="Name" formControlName="name">
  <input type="text" placeholder="Email" formControlName="email">
  
  <button type="submit">Submit</button>
  <button type="button" (click)="reset()">Cancel</button>

</form>
```


