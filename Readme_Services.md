# Services

A component is an ES6 `class {}`, plus some `@metadata ()` on top. Understanding that shape, and understanding that properties and methods on the class are available to the template, transitioning to services is really easy:

> A service is just a class

In fact, everything in Angular follows this same basic shape. **component**, **directive**, **service**, **pipe** - all consist of a `class {}` + `@metadata ()`. And more often than not, the class has nothing to do with Angular. It's the metadata that tells Angular how the thing is going to live in the application.

Three steps

* **Defining a service**

    A class and metadata. Class has methods, etc. like CRUD:

    ```ts
    @Injectable()
    export class ItemsService {
      constructor(private http: Http) { }

      loadItems() { }

      loadItem(id) { }

      saveItem(item: Item) { }

      createItem(item: Item) { }

      updateItem(item: Item) { }

      deleteItem(item: Item) { }
    }
    ```

* **Exposing a service**

    Then register it in the main module, in the `providers` array:

    ```ts
    @NgModule({
      declarations: [
        AppComponent,
        ItemsComponent,
        ItemsListComponent,
        ItemDetailComponent
      ],
      imports: [
        BrowserModule,
        FormsModule,
        HttpModule,
        Ng2RestAppRoutingModule
      ],
      providers: [ItemsService],  /*  <-- look here   */
      bootstrap: [AppComponent]
    })
    export class AppModule { }
    ```

* **Consuming a service**

    Consume a service in a component using dependency injection via constructor assignment:

    ```ts
    import { Component, OnInit }  from '@angular/core';
    import { ItemsService, Item } from '../shared';

    @Component({
      selector    : 'app-items',
      templateUrl : './items.component.html',
      styleUrls   : ['./items.component.css']
    })
    export class ItemsCOmponent implements OnInit {
      items: Array<Item>;
      selectedItem: Item;

      constructor(private itemsService: ItemsService) {}

      ngOnInit() {
        this.itemsService.loadItems()
          .then(items => this.items = items);
      }
    }
    ```

Do you always put your services in a 'shared' or common folder? It depends. If you 100% absolutely know that a service will only be used by a single component, it's safe to write that service in the same place as the component.

However, if you need to use it in more than one component (eg, using a widgets service in both a Widgets component and the Home component), it makes sense to store it in a shared folder.

Anything that communicates with a server, the author puts in a `shared` folder by default.

So what if you have like 20 services in 1 folder? No good, time to rethink them. Half art, half science, but you need to obtain a level of abstraction that makes sense.

Remember: with services, you **Create**, **Expose**, **Consume**.