# Application Complexity

Programming is complex.

>"I programmed for 10 years before I learned Lisp. And then I realized that I didn't know anything." -- Jaffar Huessein

>The biggest problem in the development and maintenance of large-scale software systems is **complexity** -- large systems are hard to understand. -- "Out of the Tarpit", Ben Mosely & Peter Marks

What do they mean by complexity? Mosely and Marks define complexity in therms of three things:

>We believe that the major contributor to this complexit in many systems is the **handling of state** and the burden this adds when trying to analyse and reazon about the system. Other closely related contributors are **code volume** and explicit concern with the **flow of control** through the system.

Occasionally a system appears complex because the code is ambiguous. You can't tell what it does.

### Complexity and Purgatory

Purgatory: the feeling of dread when you're overwhelmned by some code. Author argues this is most often a result of code complexity.

Consider this code:

```ts
class ItemsComponent {
  total: Number = 0;
  currentCategory: string = 'cool';
  currentAgeGroup: string = 'child';

  inbound(item) {
    const newTotal: number;
    switch(this.currentCategory) {
      case 'fun':
        /* calculate total based on fun factor */
        break;
      case: 'cool':
        /* calculate total based on cool factor */
        break;
      case: 'dangerous':
        if(this.currentAgeGroup !== 'child') {
          /* calculate total based on danger factor */
          this.currentCategory = 'dangerous';
        } else {
          /* calculate total based on alterntate danger factor */
        }
        break;
      default:
        /* Do nothing at all */
    }
    return newTotal;
  }
}
```

The above component method `inbound` refers to "hidden state" `currentCategory` and `currentAgeGroup` defined on the class. So we can't be sure what will happen when calling the class method.

```ts
const itemsComponent = new ItemsComponent();
const myItem = { name: 'My Item' };

itemsComponent.inbound(myItem);  // some result

itemsComponent.currentCategory = 'fun';  // changing state
itemsComponent.inbound(myItem);  // same parameter, different  result

itemsComponent.currentCategory = 'cool';  // changing state
itemsComponent.inbound(myItem);  // same parameter, different  result

itemsComponent.currentCategory = 'dangerous';  // changing state
itemsComponent.inbound(myItem);  // same parameter, different  result
```

You have to know somehow that you need to control that hidden category. Humans are not that good at keeping lots of things like this in mind. Complexity leads to purgatory.

### Controlling Flow

Borrowing some AngularJS logic:

```js
function doWork() {
  return $http.post('url')
    .then(function(response) {
      if (response.data.success) {
        return response.data;
      } else {
        return $q.reject('some error occurred');
      }
    })
}
doWork().then(console.log, console.error);
```

What happens when you're making a call with a promise and it fails and you need to recover and do it again? The only way to do that in AngularJS is to introduce more code.

```js
function doWork() {
  return $http.post('url')
    .then(function(response) {
      if (response.data.success) {
        return response.data;
      } else {
        return $q.reject('some error occurred');
      }
    })
    .then(null, function(reason) {
      if (retriesCount++ < 3) {
        return doWork();
      } else {
        return $q.reject(reason);
      }
    })
}
doWork().then(console.log, console.error);
```

Cycle of complexity: lacking a proper mechanism, we introduce more code volume and some hidden state (`retriesCount`), and adding hidden state means more code volume.