# Make it Right

**Static Analysis**

Step 1: lint

`ng lint`

Step 2: Code Coverage

`ng test -cc`

>This does not work for me.

Step 3: Source Map Explorer

```
sudo npm install -g source-map-explorer
ng build
source-map-explorer ./dist/vendor.*
```

