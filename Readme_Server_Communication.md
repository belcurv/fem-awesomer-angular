# Server Communication

The HTTP Module in Angular simplifies the use of the XHR and JSONP APIs by using RESTful verbs and returning an observable object. Lucas takes a look at some the methods available in the API.

### The HTTP Module

* Simplifies usage of the XHR and JSONP APIs
* API conveniently matches RESTful verbs
* Returns an **observable**

### HTTP Module Methods

* `request`: performs any type of http request
* `get`: performs a request with GET http method
* `post`: performs a request with POST http method
* `put`: performs a request with PUT http method
* `delete`: performs a request with DELETE http method
* `patch`: performs a request with PATCH http method
* `head`: performs a request with HEAD http method

Examples from `ItemsService`:

```ts
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';

loadItems() {
  return this.http
    .get(BASE_URL)
    .map(res => res.json())
    .toPromise();
}

createItem(item: Item) {
  return this.http
    .post(BASE_URL, JSON.stringify(item), HEADER)
    .map(res => res.json())
    .toPromise();
}

updateItem(item: Item) {
  return this.http
    .put(`${BASE_URL}${item.id}`, JSON.stringify(item), HEADER)
    .map(res => res.json())
    .toPromise();
}

deleteItem(item: Item) {
  return this.http
    .delete(`${BASE_URL}${item.id}`, JSON.stringify(item), HEADER)
    .map(res => res.json())
    .toPromise();
}
```

What's `.toPromise()` in the above? It converts the **observable** to a promise. We can chain any HTTP method (or any observable) with `.toPromise()`. Then we can use `.then()` and `.catch()` to resolve the promise as usual.

Note also that `.map()` in the above is not the traditional array method. Instead it's a part of RxJS and is used to convert the response into JSON.

Elsewhere in our app, where we use the service:

```ts
constructor(private itemsService: ItemsService) {}

ngOnInit() {
  this.itemsService.loadItems()
    .then(items => this.items = items);
}
```

### Observables

`Observable.subscribe()` - kind of the equivalent of `Promise.then()`.

* We finalize an observable stream by subscribing to it
* The `subscribe` method accepts three event handlers:
  1. `onNext` - called when new data arrives
  2. `onError` - called when and error is thrown
  3. `onComplete` - called whem the stream is completed

Converting the `loadItems` service method from above, just leave off the bit where we convert it into a promise:

```ts
loadItems() {
  return this.http.get(BASE_URL)
    .map(res => res.json())
}
```

And in the consuming component we call `.subscribe()` (only the success handler is shown below):

```ts
constructor(private itemsService: ItemsService) {}

ngOnInit() {
  this.itemsService.loadItems()
    .subscribe(
      // onNext handler
      items => this.items = items
    ):
}
```

### Headers

* Https module methods have an optional 2nd parameter which is a `RequestOptions` object.
* The `RequestOptions` object has a headers property which is a `Headers` object
* We can use the `Headers` object to set additional parameters like `Content-Type`, etc.

Example:

```ts
import { Http, Headers } from '@angular/http';
import { Injectable } from '@angular/core';
import { Item } from './item.model';
import 'rxjs/add/operator/map';

const BASE_URL = 'http://localhost:3000/items/';
const OPTIONS = {
  headers: new Headers({ 'Content-Type': 'application/json' })
};

@Injectable()
export class ItemsService {

  constructor(private http: Http) {}

  createItem(item: Item) {
    return this.http
      .post(BASE_URL, JSON.stringify(item), OPTIONS)
      .map(res => res.json())
  }

}
```

### Error Handling

First contact with errors happens at the service level. 

* Use `Observable.catch` to process the error at the service level. From here, you decide what you want to do. If you want to surface the error, call `Observable.throw`:
* Use `Observable.throw` to force an error further down the stream.
* Use the error handler in the `subscribe()` method to respond to the error at the component level.

This level of control is not possible with promises.

Process the error in the service:

```ts
loadItem(id) {
  return this.http
    .get(`${BASE_URL}${id}`)
    .map(res => res.json())
    .catch(error => Observable.throw(error.json().error || 'Server error'));
}
```

Handle the error in the component:

```ts
ngOnInit() {
  this.itemsService.loadItems()
    .map(items => this.items = items)
    .subscribe(
      this.diffFeaturedItems.bind(this),  // onNext
      this.handleError.bind(this)         // onError
    );
}
```

