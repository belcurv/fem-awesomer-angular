# Template Fundamentals

If a component is just an ES6 class, a template is, for the most part, just HTML.

<Template>-@metadata-{class} sandwich.

Between the template and class, we can have `{{string interpolation}}`, `[property]` binding, `(event)` binding, and 2-way `[(ngModel)]` bindings.

**String Interpolation**

```html
<p>{{ color }}</p>
```

**Property Binding**

* Flows data from the component to an element
* Created with brackets: `<img [src]="image.src" />`
* The canonical form of `[property]` is **bind-property**
* There are special cases for binding to attributes, classes, and styles that look like `[attr.property]`, `class.className`, and `style.styleName` respectively

Example: Property Binding

```html
<h3 [style.color]="color">My text will be color: {{ color }}</h3>
```

**Event Binding**

Capturing user events and calling methods defined on our class.

* Flows data from an element to the component
* Created with parentheses `<button (click)="foo($event)"></button>`
* The canonical form of `(event)` is `on-event`
* Information about the target event is carried in the `$event` parameter

Example:

```html
<button
  type="button"
  md-button (click)="echo(color)">
  Holla!
</button>
```

**Two-way Binding**

Binds to some class property, reading from it as well as updating it.

* Really just a combination of property and event bindings
* Used in conjunction with `ngModel`
* Referred to as "banana in a box"

Examples:

```html
<input
  mdInput
  [(ngModel)]="color"
  type="text"
  name="color">
```

```html
<label>The awesome input</label>
<input type="text" [(ngModel)]="dynamicValue" placeholder="Watch the text update" />
<label>The awesome output</label>
<span>{{ dynamicValue }}</span>
```

**Local Template Variables**

What happens when you have an element on a page that you want to reference in some other element? Use cases: if you're doing UI with observables - you have a mouse move and want to create a new observable, or you want to drag something. So you need a reference to that element on the stage.

* The hashtag (#) defines a local variable inside our template
* We can refer to a local template variable _anywhere_ in the current template
* To consume, simple use it as a variable without the hashtag
* The canonical form of `#variable` is `ref-variable`

For our purposes, we'll use this to reference a form. When working with forms, you have the underlying data (what's in the fields) and you have the validity of that data. So we need a way to say, if the form is not valid, disable this button.

You will see this pattern quite a bit:

```html
<form novalidate #formRef="ngForm"
  
  <label>Item Name</label>
  <input [(ngModel)]='selectedItem.name"
    type="text" name="name" required
    placeholder="Enter a name">

  <label>Item Description</label>
  <input [(ngModel)]="selectedItem.description"
    type="text" name="description"
    placeholder="Enter a description">
  
  <button type="submit"
    [disabled]="!formRef.valid"
    (click)="saved.emit(selectedItem)">
    Save
  </button>
```

**Attribute Directives**

**Structural Directives**

* A structural directive changes the DOM layout by adding or removing DOM elements
* Asterisks indicate a directive that modifies the DOM
* It is syntactic sugar to avoid having to use template elements directly

Example:

```html
<div *ngIf="hero">{{ hero }}</div>

<div *ngFor="let hero of heros">{{ hero }}</div>

<span [ngSwitch]="toeChoice">
  <span *ngSwitchCase="'Eenie'">Eenie</span>
  <span *ngSwitchCase="'Meanie'">Meanie</span>
  <span *ngSwitchCase="'Miney'">Miney</span>
  <span *ngSwitchCase="'Moe'">Moe</span>
  <span *ngSwitchDefault>other</span>
</span>
```

Note - doing the above longhand (without syntactic sugar) would look like this:

```html
<span [ngSwitch]="toeChoice">
  <template [ngSwitchCase]="'Eenie'"><span>Eenie</span></template>
  <template [ngSwitchCase]="'Meanie'"><span>Meanie</span></template>
  <template [ngSwitchCase]="'Miney'"><span>Miney</span></template>
  <template [ngSwitchCase]="'Moe'"><span>Moe</span></template>
  <template ngSwitchDefault><span>other</span></template>
</span>
```

**Safe Navigation Operator**

* If you reference a property in your template that does not exist yet in your class, you will throw an exception.
* The safe nagivation operator is a simple, easy way to guard against null and undefined properties
* Denoted by a question mark immediately followed by a period: `?.`

For example, if `firstName` does not exist on `nullHero` but we still need to bind to it, we just put a `?` after it.

```html
<!-- no hero, no problem -->
The null hero's name is {{ nullHero?.firstName }}
```
