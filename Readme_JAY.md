# Day 1

Marcus = @simpulton on Twitter and Github

90% of Angular consists of these 4 things:

1. module
2. routes
3. component
4. services

### Module

Creates containers that say "I need these things, and I'm making available this thing to the rest of the world."

#### ES6 Modules

* ES6 modules provide organization at a **language** level
* Useing ES6 module syntax
* Modules export things that other modules can import

```ts
import { Component, OnInit } from '@angular/core';
import { ItemsServices, Item } from '../shared';

export class ItemsComponent implements OnInit {}
```

#### @NgModule

A map or manifest describing how a thing is organized.

* provides organization at a **framework** level
* **declarations** define _view classes_ that are available to the module
* **imports** define a list of modules that the module needs
* **providers** define a list of services the module makes available
* **bootstrap** defines the component that should be bootstrapped. What is the entry point.

```ts
@NgModule({
  declarations: [
    AppComponent,
    ItemsComponent,
    ItemsListComponent.
    ItemDetailComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    Ng2RestAppRoutingModule
  ],
  providers: [
    ItemsService
  ],
  bootstrap: [
    AppComponent
  ]
})
export class AppModule { }
```

### Routes

Routing - ways to navigate to specific components within application.

* Routes are defined in a route definition table that in its simplest form contains a **path** and a **component** reference
* Components are loaded into the **router-outlet** component
* We can navigate to routes using the 88routerLine** directive
* The router uses **history.pushState** which means we need to set a **base-ref** tag to our **index.html** file

```ts
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ItemsComponent } from './items/items.component';

const routes: Routes = [
  { path: '',      redirectTo: '/items', pathMatch: 'full' },
  { path: 'items', component: ItemsComponent' },
  { path: '**',    redirectTo: '/items', pathMatch: 'full' }
];

@NgModule({
  imports   : [RouterModule.forRoot(routes)],
  exports   : [RouterModule],
  providers : []
})
export class Ng2RestAppRoutingModule { }
```

### Components

The atomic building block of angular. Should be single-purposed, fine-grained. Not hundreds of lines of code.

Component consists of 2 things: a **template** and a **class**. Akin to Angular 1.x controllers. Really 3 things: **template**, **class** and **metadata** - more on that later.

Component Classes

* Components are just ES6 classes
* Properties and methods of the component class are available to the template
* Providers (Services) are injected in the constructor. Dependency injection allows you to mock dependencies during testing.
* The component lifecycle is exposed with hooks

```ts
export class ItemsComponent implements OnInit {
  items: Array<Item>;
  selectedItem: Item;

  constructor(private itemsService: ItemsService) {}

  ngOnInit() {
    this.itemsServie.loadItems()
     .then(items => this.items = items):
  }
}
```

Class methods and properties can be bound to.

Component Templates

* A template is HTML that tells Angular how to render a component
* Templates include data bindings as well as other components and directives
* Angular leverages native DOM events and properties which dramatically reduces the need for a ton of built-in directives. If it's a DOM event, you can bind to it.
* Angular leverages shadow DOM to do some really interesting things with view encapsulation. CSS is no longer _cascading_ style sheets, it's now _component_ style sheets.

```ts
@Component({
  selector: 'app-items-list',
  templateUrl: './items-list.component.html',
  styleUrls: ['./items-list.component.css']
})
export class ItemsListComponent {
  @Input() items: Item[];
  @Output() selected = new EventEmitter();
  @Output() deleted = new EventEmitter();
}
```

### Metadata

* Metadata allows Angular to _process_ a class, inserting it into the Angular app lifecycle
* We can attach metadata with TypeScript using **decorators**
* Decorators are just functions
* Most common is the `@Component()` decorator
* Takes a config option with the selector, templateUrl, styles, styleUrls, animations, etc.

The following says, we have a component, we're saying take this class, attach this template and this css, and when we attach it to the DOM, do so using the 'app-items' selector.

```ts
@Component({
  selector: 'app-items',
  templateUrl: './items.component.html',
  styleUrls: ['./items.component.css']
})
export class ItemsComponent implements OnInit { }
```

You can also decorate properties and methods:

```ts
@Component({
  selector: 'app-items-list',
  templateUrl: './items-list.component.html',
  styleUrls: ['./items-list.component.css']
})
export class ItemsListComponent {
  @Input() items: Item[];
  @Output() selected = new EventEmitter();
  @Output() deleted = new EventEmitter();
}
```

### Data Binding

How do we communicate between the class and template. Four main ways:

1. `{{ value }}` - String interpolation. Renders data to template. One-way binding.

2. `[property] = "value"` - when you have a value in a component class that you want to send to your template, you do so using property binding. One-way binding.

3. `(event) = "handler"` - if something happens in your template and you want to communicate that back to your class, you do so using event binding. One-way binding.

4. `[(ngModel)] = "property"` - Combines both "ways" into a two-way binding. "Banana in a box". "Angular hug".

```html
<h1>{{title}}</h1>
<p>{{body}}</p>
<hr/>
<experiment *ngFor="let e of experiments" [experiment]="e"></experiment>
<hr/>
<div>
  <h2 class="test-error">Experiments: {{message}}</h2>
  <form class="form-inline">
    <input type="text" [(ngModel)]="message" placeholder="Message">
    <button type="submit" class="btn" (click)="updateMessage(message)">Update Message</Button>
  </form>
</div>
```

### Directives

What about them?! 

* A directive is a class decorated with **@Directive**
* A component is just a directive with added template features.
* Built-in directives include structural directives and attribute directives.

A directive is kind of a super set of a component, in that it provides functionality to an element but it doesn't provide the template for it. It takes an existing element and augments it. It's behavior only. Not behavior and view.

`ngIf`, `ngFor`.

```ts
import { Directive, ElementRef } from '@angular/core';

@Directive({ selector: 'blink' })
export class Blinker {
  constructor(element: ElementRef) {
    // magic
  }
}
```

### Services

* A service is _generally_ just a class
* SHould do only 1 specific thing
* Take the burden of business logic out of components
* It is considered best practice to always use **@Injectable** so that metadata is generated correctly

```ts
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';

const BASE_URL = 'http://localhost:3000/items/';

@Injectable()
export class ItemsService {
  constructor(private http: Http) {}

  loadItems() {
    return this.http.get(BASE_URL)
      .map(res => res.json())
      .toPromise();
  }
}
```

### TypeScript

TypeScript things that are not in ES6.

1. Field Assignment - You can define your properties outside of a constructor. "Here's some properties, attach them to the class." You cannot do this in ES6 (these have to go in the constructor).

    ```ts
    export class ItemsComponent implements OnInit {
      items: Array<Item>;   // <-- field assignment
      selectedItem: Item;   // <-- field assignment
      ...
    ```

2. typings - Communicate intent, both to other devs and also to your IDE. We can say `items` and `selectedItem`, well, they're of type 'Item'.

3. interfaces - the `implements` keyword, says that a class has to behave a certain way. As long as something adheres to an interface, or honors a contract, you don't really care what it is in a concrete sense. You just know that you're getting this thing, and it honors this contract, and therefore there's this expected behavior. In other words, although the actual implementation may differ each time, I know that the behavior will be the same.

4. constructor assignment - in the constructors params, by adding in an access modifier such as 'private' or 'public', TypeScript knows this is an instance on the class and adds the params for you.

    ```ts
      ...
      constructor(private itemsService: ItemsService) {}

      ngOnInit() {
        this.itemsService.loadItems()
        .then(items => this.items = items);
      }
    }
    ```

So under the hood, it's basically saying `this.itemsService = ItemsService`, which then allows you to use it outside of the constructor.

## Challenge 1: Angular Pieces

* Identify the major Angular pieces in the sample application
* Add a new property to the Items component and bind to it in the view
* Add a new property to the ItemsService and consume it in a component

`main.ts` - bootstraps out main module (`app.module.ts`), and then within out main module, we bootstrap the main component (`app.component.ts`).

### Wrap up

Module - a broad container.

Routes - define how we get to specific pieces/components.

Components - thin layers that users can interact with.

Services - business logic and state management.

`index.html` - includes a tag `<app-root>` - this selector refers to the top-level component. This is how Angular begins to live on the page.

`main.ts` - bootstraps the application:

```ts
platformBrowserDynamic().bootstrapModule(AppModule);
```

That refers to ...

`AppModule`, the default export of `app.module.ts`. `AppModule` bootstraps `AppComponent` in `app.component.ts` - see the `app-root` selector in the decorator?:

```ts
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Angular REST App';
  links = [
    { path: '/home', icon: 'home', label: 'Home'},
    { path: '/items', icon: 'list', label: 'Items'}
  ];
}
```

`main.ts` bootraps the main module, and within our main module, we bootsrap the main component.

`app.component.ts` includes a `<router-outlet>` tag - this is where other components will be inserted. You can also have nested routes by including router outlets in child components.

## Angular CLI

Streamlines a lot of the build tool fatigue.

```bash
npm install -g @angular/cli

ng new my-dream-app

cd my-dream-app

ng serve
```

Initially we won't use the CLI generators for components, so that we learn how the pieces work.

The CLI gives us:

* Fully function project generation that just works
* Code generator for components, directives, pipes, enums, classes, modules and services
* Build generator
* Unit test runner
* End-to-end test runner
* App deployment to GH Pages
* Linting
* CSS preprocessor support
* Ahead of time (AOT) compilation support
* Lazy routes
* Extensible blueprints coming soon

Generate a component:

```bash
ng generate component my-new-component
```

Alias shortcut:

```bash
ng g c my-new-component
```

If we have multiple modules, we have to specify what module to attach a component to during generation:

```bash
ng g c my-new-component -m app.module.ts
```

What about nesting child components?  Say we have an `items` component and we want a nested child `item` component. We can do this in one move with the CLI, but the syntax is a little tricky. We have to specify the path to the child component `items/item` and then _back out as many levels as needed_ to get to the path of the module we want to register the component on:

```bash
ng g c items/item -m ../app.module.ts
```

Generate a module:

```bash
ng g m my-new-module --routing
```

Services, same thing:

```bash
ng generate service my-new-service
ng g s my-new-service # using the alias
```

Production build:

```bash
ng build
```

Testing:

```bash
ng test
ng e2e # end-to-end
```

Linting:

```bash
ng lint
```

Test a CLI command without actually executing it:

```bash
ng g m refreshments-module --routing --dry-run
```

Deploy to GH Pages:

```bash
ng github-pages:deploy --message "optional commit message"
```

## Component Fundamentals

### Anatomy of a Component

* `<template>`
* `class{}`
* `@metadata`
* `(event binding)`
* `[property binding]`

**Soapbox:** Class !== Inheritance. Author worried about using "classes" and inheritance. But in JS, we don't intrinsically need to use inheritance, we can consider classes as basic objects or containers. Author has never met a case where he needed to use inheritance - where 'composition' via dependency injection did not work.

### CIDER mnemonic - _Class, Import, Decorate, Enhance, Repeat_

1. Class Definition:

    ```ts
    export class ItemsComponent {}
    ```

2. Import your dependencies:

    * Import the core Angular dependencies
    * Import 3rd party dependencies
    * Import your custom dependencies
    * This approach gives us fine-grained control over dependency management

    ```ts
    import { Component } from '@angular/core';
    export class ItemsComponent {}
    ```

3. Decorate your class:

    * We turn out class into something Angular can use by decorating it with Angular specific metadata
    * Use the `@Component` syntax to decorate your classes
    * You can also decorate properties and methods within your classes
    * The two most common member decorators are `@Input` and `@Output`

    ```ts
    import { Component } from '@angular/core';

    @Component({
      selector    : 'app-items',
      templateUrl : './items.component.html',
      styleUrls   : ['./items.component.css']
    })
    export class ItemsComponent {}
    ```

4. Enhance it with properties and methods:

    ```ts
    import { Component } from '@angular/core';
    import { Item } from '../shared';

    @Component({
      selector    : 'app-items',
      templateUrl : './items.component.html',
      styleUrls   : ['./items.component.css']
    })
    export class ItemsComponent {
      /* PROPERTIES */
      items: Array<Item>;
      selectedItem: Item;

      constructor() {}

      /* METHODS */
      resetItem() {
        let emptyItem: Item = { id: null, name: '', description: '' };
        this.selectedItem = emptyItem;
      }

      selectItem(item: Item) {
        this.selectedItem = item;
      }
    }
    ```

    Injecting a dependency. Say your component class needs a service, you simply pass it in using a `private` modifier:

    ```ts
    import { Component, OnInit } from '@angular/core';
    import { Item } from '../shared';

    @Component({
      selector    : 'app-items',
      templateUrl : './items.component.html',
      styleUrls   : ['./items.component.css']
    })
    export class ItemsComponent implements OnInit {
      /* PROPERTIES */
      items: Array<Item>;
      selectedItem: Item;

      constructor(private itemsService: ItemsService) {}

      ngOnInit() {
        this.itemsService.loadItems()
          .then(items => this.items = items);
      }

    }
    ```

5. Repeat the above

    When a component grows too big, refactor it into smaller child components. Rinse, repeat.

### Lifecycle Hooks

An anti-pattern is to have complex logic in constructors, the reason being, how do you call a constructor on a class? You don't - it's implicity called. So don't tuck logic into something can can't explicitly call.

* Allow us to perform custom logic at various stages of a component's life.
* Data isn't always immediately available in the constructor - that data may be en route from an async / ajax call.
* Implemented as class methods on the component class.
* The lifecycle interfaces are optional. We recommend adding them to benefit from TypeScript's strong typing and editor tooling.

Available hooks

* `ngOnChanges` - called when an input or output binding value changes
* `ngOnInit` - called after the first ngOnChanges
* `ngDoCheck` - handles the developer's custom change detection
* `ngAfterContentInit` - called after component content initialized
* `ngAfterContentChecked` - called after every check of component content
* `ngAfterViewInit` - called after component's view(s) are initialized
* `ngAfterViewChecked` - called after every check of a component's view(s)
* `ngOnDestroy` - called just before the directive is destroyed

The two most common: 

* `ngOnInit`
* `ngOnDestroy`

And if a component is "stateless", you really only need `ngOnInit`. Angular is "very good about cleaning up after itself".



**cool example app**

If the author were to write a real application that would scale, this is pretty much look like this:

https://github.com/ngrx/platform#examples

This course will not get into Redux. Author has another that does. Author prefers ngrx over Redux because ngrx is based on observables.

# Day 2

**Intro review** Goal: expand what we learned about features (a component and a service) and extend that knowledge to how to construct multiple features/components and get them to work together. Make sure we have a string grasp on how to construct and compose features in angular.

Purpose of `NgModule`: an organizational mechanism at the framework level. It's like a manifest that loads dependencies, other modules, etc. that tells Angular how to wire itself up, and which top-level module to bootstrap.

Purpose of a regular ES6 module: it's a container, a languale-level organizational mechanism, allows you to organize your app into blocks that can then, using webpack for ex., get put back together.

Purpose of routes: a way to switch to different application states/features/views/pages. Matches a URL to a specific state in your application.

Role of components: atomic building block of your application, consisting of a minimum of a class and a template. Encapsulate a bit of functionality so we can use it wherever we want. Components should consume just enough data to satisfy their templates, and capture user events & route to services.

Role of services: units of business logic for your application. Domain models. Shared logic. Services should be rich.

`ngOnInit() { ... }` is a lifecycle hook (an event in time), happens after the constructor, fires when the initial state is ready, which means ngOnChanges has fired at least once (so basically all the bindings have been triggered at least once). This is where binding-dependent logic should go - not in the constructor.

How do we inject a dependency into our component? Using constructor assignment:

```ts
constructor(private itemsService: ItemsService) {}
```

Why the `private` keyword here? Under the hood, this assigns the `ItemsService` to a local class property `itemsService`. So the above class would look this this in ES5:

```js
var Greeter = (function () {
  function MyClass(itemService) {
    this.itemService = itemService;
  }
  return MyClass;
}());
```

A component decorator must have a selector and a template. Styles are optional.

## Part 3

Agenga

* Review Challenge
* Application Complexity
* Immutable Operations
* Reactive Forms
* Event Communication
* Route Parameters
* Angular and Firebase

* Make it Work - testing and debugging
* Make it Right - Static Analysis
* Make it Fast - Angular performance
* Make it Live - Deploying

Review

Communicating ... state flows 'down', events flow 'up':

1. from a component class to its template = [property] binding in template.
2. from a component template to its class = (event) binding in template.
3. from parent component template to child class = [property] binding in parent template and `@Input` in child class.
4. from child component class to parent template = (event) binding in parent template and `@Output` in child class.

Observables:

Output pushed to input. There's output happening. You decide where you want to hook into that output, and to what input(s) you want to direct the stream.

Talk "Everything is a Stream": https://www.youtube.com/watch?v=UHI0AzD_WfY

To consume an observable stream, you `.subscribe()` to it.

```ts
ngOnInit() {
  const search$ = Observable
    .fromEvent(this.getNativeElement(this.itemSearch), 'keyup')
    .debounceTime(200)
    .distinctUntilChanged()
    .map((event: any) => event.target.value)
    .switchMap(term => this.itemsService.search(term))
    .subscribe(items => this.onResults.emit(items));
}
```

As of RxJS 5.5 I believe the syntax is:

```ts
ngOnInit() {
  const search$ = fromEvent(this.getNativeElement(this.itemSearch), 'keyup')
    .pipe(
      debounceTime(200),
      distinctUntilChanged(),
      map((event: any) => event.target.value),
      switchMap(term => this.itemsService.search(term))
    )
    .subscribe(items => this.onResults.emit(items));
}
```

### Destructuring Parameters in TypeScript

Typing the destructured object looks kinda strange:

```ts
subscribe({ value, valid }: { value: SomeInterface, valid: boolean }) {
  console.log(value, valid);
}
```

## Make it Work, Make it Right, Make it Fast

Make it work first - get it in front of the people who matter and get feedback. The faster you get something in place to get that feedback loop, the better off you're going to be.
