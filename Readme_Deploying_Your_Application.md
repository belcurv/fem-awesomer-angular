# Deploying an Application

Angular CLI defaults mean that these commands are equivalent:

```bash
ng build --target=production --environment=prod
ng build --prod --env=prod
ng build --prod
```

So are these:

```bash
ng build --target=development --environment=dev
ng build --dev --e=dev
ng build --dev
ng build
```

By default, `ng build` thinks you're in a development environment, targeting development.

### Development vs Production Build Differences

|                |    dev    |   prod   |
| -------------- | --------- | -------- |
| aot*           |   FALSE   |   TRUE   |
| environment    |   dev     |   prod   |
| output-hashing |   media   |   all    |
| sourcemaps     |   TRUE    |   FALSE  |
| extract-css    |   FALSE   |   TRUE   |
| named-chunks   |   TRUE    |   FALSE  |

* `aot` refers to Angular's Ahead-of-Time Compiler - converts your Angular HTML and TypeScript code into efficient JavaScript code during the build phase, before the browser downloads and runs that code.

### Benefits of AOT

* smaller payload
* fewer async requests
* faster rendering - it's been optimized for specifically what the app needs to do.