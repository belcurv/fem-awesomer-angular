# Interfaces

For example, lets create a widgets interface: `src/app/shared/widgets.model.ts`:

```ts
export interface Widget {
  id: number;
  name?: string;
  description?: string;
}
```

Then we can use it to 'type' our properties within the widgets component class:

```ts
export class WidgetsComponent implements OnInit {

  // interface! Typings!
  selectedWidget: Widget;
  widgets: Array<Widget>; // aka: widgets: Widget[];
```