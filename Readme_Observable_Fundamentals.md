# Observable Fundamentals

Reactive Extensions: http://reactivex.io/rxjs/

Check out this incredible site: https://www.learnrxjs.io/

An observable is a lazy event stream which can emit events. A _subject_ performs some logic and notifies the _observer_ at the appropriate times.

With Promises, something is going to happen in the future. You get a Promise object that defers action to the future, when this thing eventually happens.

Observables work similarly, except that instead of getting just one object you can get many objects. It's a stream of data that you can subscribe to.

Distinguish observable stream itself from all the various operators.

"Observable" is actually the combination of two classid design patterns: **iterator pattern**, and **observer pattern**.

Iterator pattern - designed to communicate state. Give me the next value. Give me the next value. Give me the next value. You can pull state off an iterator object.

Observer pattern - good for communicating that somethign has happened in the future.

Combining them, we can **communicate state over time**. This is really handy in an async environment (like JS).

Unlike promises, which resolve and 'end', observables communicate multiple values over time. As time progresses, we have events that we can capture and communicate to our consumers. Promises push a result value to a handler function. Observables work similarly: when something happens, the observable stream will push the event to consumers.

Main game-changing idea: multiple values, pushed to you, over time.

### Challenge 10: ItemSearch -- pre-challenge

* Create and `ItemsSearch` component
  ```
  ng g c ./items/items-search -m ../app.module.ts
  ```
* Add the `ItemsSearch` component to the `HomeComponent` template
* Go to `http://bit.ly/items-search-snippets` and copy the snippets to the component

### Basic Observable Sequence

We're familiar with the I/O concept of, like: "from input, get output". I'm going to give you this thing, and you're going to give me something back".

```
[ INPUT ] --> [ OUTPUT ]
```

Author proposes we invert this:

```
[ OUTPUT ] --> [ INPUT ]
```

With observables, the output is happening - there is a stream outputting data whether you're connected to it or not - and you need to figure out where you're going to put it and in what form in needs to be when it gets there.

Once the stream hits your `.subscribe()` block, it's in its final form. The observable can no longer do anything with that data. So, use observable operators to make sure your data is shaped into its final form by the time it gets to this point.

Flow looks like:

```
[ event ] --> [ operator(s) ] --> [ subscribe ]
```

1. **event** - the start of the stream; the "initial output"
2. **operators** - tons of methods to control / shape what comes out of the stream when it arrives at its "final input"
3. **subscribe** - where the stream ends; the "final input"

>User interactions are the ultimate streams. Your browser DOM is emitting events constantly. All interactions, mouse movements, etc. It's a constant stream of data that you can capture.

```ts
@ViewChild('btn') btn;
message: string

getNativeElement(element) {
  return element._elementRef.nativeElement;
}

ngOnInit() {
  Observable.fromEvent(this.getNativeElement(this.btn), 'click')
    .filter(event => event.shiftKey)       // only allow events if shift is pressed
    .map(event => 'Beast mode activated')  // get an event, swap in a message
    .subscribe(result => this.message = result);
}
```

### RxJS API changes as of v5.5

As of RxJS 5.5, the whole module and operator import syntax has changed, as have some implementation methods.

In course project code, which uses 5.4.x, we have to import stuff at the top of the file like this:

```ts
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/fromEvent';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/filter';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/operator/switchMap';```
```

As of 5.5 that's now:

```ts
import { fromEvent } from 'rxjs/observable/fromEvent';
import { map, filter, debounceTime, distinctUntilChanged, switchmap } from 'rxjs/operators';
```

And we omit the `Observable` from the above. You just start with the `fromEvent` function (or any of the [observable creation functions](https://www.learnrxjs.io/operators/creation/)). And you chain the `.pipe()` method, passing in any/all filtering / transformation methods:

```ts
const search$ = fromEvent(this.getNativeElement(this.itemsSearch), 'keyup')
  .pipe(
    debounceTime(200),
    distinctUntilChanged(),
    filter(),
    map(),
    etc()
  )
  .subscribe()
```

### Maintaining State within a stream

http://rxjsftw.in/#statemaintaining-state

The RxJS `.scan()` operator works like the array method `.reduce()`. It's "accumulator" parameter works just like `.reduce()`'s accumulator.

```ts
import { Component, OnInit, ViewChild } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/fromEvent';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/scan';
import 'rxjs/add/operator/startWith';

interface Coordinate {
  x: number,
  y: number
}

@Component({
  selector: 'app-maintaining-state',
  template: `
  <button #right md-raised-button color="accent">Move Right</button>
  <div class="container">
    <div #ball class="ball"
      [style.left]="position.x + 'px'"
      [style.top]="position.y + 'px'">
    </div>
  </div>
  `
})
export class MaintainingStateComponent implements OnInit {
  @ViewChild('right') right;
  position: any;

  ngOnInit() {
    Observable.fromEvent(this.getNativeElement(this.right), 'click')
      .map(event => 10)
      .startWith({x: 100, y: 150})
      .scan((acc: Coordinate, curr) => Object.assign({}, acc, {x: acc.x + curr}))
      .subscribe(position => this.position = position);
  }

  getNativeElement(element) {
    return element._elementRef.nativeElement;
  }
}
```

