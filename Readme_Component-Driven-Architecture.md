# Component Driven Architecture

Allows you to move from just _using_ the framework, to creating your own pieces, to extending the framework to be your own domain specific language - treating markup as a DSL by creating our own custom tags/properties.

Component Driven architecture allows us to take what we know about components and elevate it to create our own custom properties and events, and wire them together in a way that more truly models whatever domain we're in.

Topics:

* Clear Contract with `@Input` and `@Output`
* `@Input`
* `@Output`
* Container and Presentational Components

### A Brief History of Angular

With AngularJS, we would often start with a very small controller and a very small view. Most examples - even on the AngularJS home page - illustrate these tiny apps with tiny controllers and views.

But realistic production-scale applications are larger, and as apps grew we got ever-larger controllers and views. There was no real guidance on how to put together a large-scale AngularJS app. This was a very real problem through Angular 1.x

Two main approaches evolved to break large templates & controllers into smaller pieces:

1. named routes (via `ng-route` and/or `uiRouter`) - we could then break out application into smaller template-controller pairs.

2. custom directives - you would still kind of break your application up into template-controller pairs, but you would wrap them in a directive. So instead of targeting a named route, you might have separate `nav`, `content`, `footer`, etc directives.

React had success doing this using **components**. Angular is component-based as of Angular 2+. This solved the _structure problem_.

* Angular components are small, encapsulated pieces of software that can be reused in many different contexts.
* Angular strongly encourages the component architecture by making it easy (and necessary) to build out every feature of an app as a component.
* Angular components are self-encapsulated building blocks that contain their own templates, styles and logic so that they can be easily ported elsewhere.

This left the _communication problem_: how do components talk to each other?

**Component Contract** - unidirectional data flow between parent component and child component, made possible via `@Output` (things we want to pass from a component to its parent component) and `@Input` (things we receive from a parent component).

Child and parent components meet at the parent's template. Think about it: we instantiate child components using their selectors within the parent component's template's markup:

```html
<!-- inside parent template -->
<child-component></child-component>
```

**The child component defines what comes in and what goes out.** Parent components just pass properties to and pull events from their children, just like they would with local property and DOM event bindings.

But a property binding from a parent component to a child component is defined internally in the child as an `@Input`. And Events from the parent's perspective are defined as `@Output`s internally in the child.

**Component Contracts**

Are essentially an API

* Represent an agreement between the software developer and the software user (or between the supplier and the consumer).
* `@Input` and `@Output` define the interface of a component
* These then act as a contract to any component that wants to consume it.
* Also act as a visual aid so that we can infer what a component does just by looking at its inputs and outputs.

For example, we can look at the following and, because we know what property and event bindings are (and their direction), we can say "this list has an _input_ of `items` and _outputs_ of `selected` and `deleted`. In other words, the list API describes _properties it wants_ and _events it will emit_.

```html
<app-items-list [items]="items"
                (selected)="selectItem($event)"
                (deleted)="deleteItem($event)">
</app-items-list>
```

### `@Input`

* Allows data to flow from a parent component to a child component
* Defined inside a child component via the `@Input` decorator:

  ```ts
  @Input() someValue: string;
  ```

* Bind in the parent's template:

  ```html
  <child-component [someValue]="parentValue"></child-component>
  ```

* We can alias inputs, though this is very not recommended:

  ```ts
  @Input('alias') someValue: string;
  ```

Sample parent and child components:

```ts
/* ================== PARENT ================== */
@Component({
  selector : 'app',
  template : `
  <my-component [greeting]="greeting"></my-component>
  <my-component></my-component>  
  `
})
export class App {
  greeting: String = 'Hello from parent!';
}
```

```ts
/* ================== CHILD =================== */
@Component({
  selector : 'my-component',
  template : `
  <div>Greeting from Parent:</div>
  <div>{{ greeting }}</div>
  `
})
export class MyComponent {
  @Input() greeting: String = 'Default Greeting';
}
```

In the above _parent_ we create a `greeting` property and bind it into the parent template, passing it as `greeting` in the `my-component` child element selector. In the child, we define `greeting` as an `@Input` property; it's expected to be a string has a default value, and is property-bound to the child component's template.

### `@Output`

* Exposes an **EventEmitter** property that emits events to the parent component
* Defined inside a component via the `@Output` decorator, and using EventEmitter (allows us to create our own custom events):
  ```ts
  @Output() showValue = new EventEmitter();
  ```
* Event bind in the parent template - whenever `showValue` fires, call `handleValue()`:
  ```html
  <child-component (showValue)="handleValue()"></child-component>
  ```

```ts
/* ================== CHILD =================== */
import { Component, Output, EventEmitter } from '@angular/core';

@Component({
  selector : 'my-component',
  template : `<button (click)="greet()">Greet Me</button>`
})
export class MyComponent {
  @Output() greeter = new EventEmitter();

  greet() {
    this.greeter.emit('Child greeting emitted!');
  }
}
```

```ts
/* ================== PARENT ================== */
@Component({
  selector : 'app',
  template : `
  <div>
    <h1>{{ greeting }}</h1>
    <my-component (greeter)="greet($event)"></my-component>
  </div>
  `
})
export class App {
  private greeting;

  greet(event) {
    this.greeting = event;
  }
}
```

### Containers & presentational Components

Consider a parent component with multiple child components, that communicates with child components using `@Input` and `@Output`.

* Container components are connected to services. They consume the data that an entire feature needs. They do very little layout. They mostly just satisfy the dependencies of their children.
* Container components know how to load their own data, and how to persist changes.
* Presentational components are generally stateless and fully defined by their bindings.
* All the data does in as inputs and is immediately rendered in their templates, and every change comes out as an output back to the containing component.
* Create as few container components & as many presentational components as possible.

Author typically likes to organize his applications by feature, and creates a route for every feature. Each route loads a top-level container component (see `items` in source code). It communicates data / events to child presentational components.

Example of a nice and sparse **presentational component** class - note that there is **zero** logic in this component:

```ts
export class ItemsListComponent {
  @Input()  items: Item[];
  @Output() selected = new EventEmitter();
  @Output() deleted  = new EventEmitter();
}
```

How would you unit test the above?! How do you test a component that has no logic?! You don't. You should still e2e or integration test, but there's nothing to unit test. You Unless Angular itself is broken, or you're passing in some bad data, this component will work the same way every time. Presentational components like this reduce the footprint of something going wrong.

Example of a **container component** class:

```ts
export class ItemsComponent implements OnInit {
  items: Array<Item>;
  selectedItem: Item;

  constructor(private itemsService: ItemsService) {}

  ngOnInit() { }

  resetItem() { }

  selectItem(item: Item) { }

  saveItem(item: Item) { }

  replaceItem(item: Item) { }

  pushItem(item: Item) { }

  deleteItem(item: Item) { }
}
```

The container is responsible for pulling in the items service and communicating with that service to persist changes to the back end.

**Don't do shared mutable state**

Create copies of objects if/when they can be modified. For example, see `item-detail.component.ts`:

```ts
export class ItemDetailComponent {
  originalName: string;
  selectedItem: Item;
  @Output() saved = new EventEmitter();
  @Output() cancelled = new EventEmitter();

  @Input() set item(value: Item){
    if (value) { this.originalName = value.name; }
    this.selectedItem = Object.assign({}, value);
  }
}
```

Why do we use a setter with `@Input() set item` instead of just assigning `Item` directly? Because since an item is an object and passed by reference, as a user would edit the item details, that Item would get updated immediately everywhere.

But using a setter and `Object.assign()` we can make a copy of the Item object while retaining the original for rpesentational reasons and in case we need to roll edits back. Mutations (edits) are restricted to just this component until we call `saved` and emit changes out.

About that `set` ... Every time this component receives an input `item`, the method is called which sets the original value and set `selectedItem` to a copy of the passed in item.